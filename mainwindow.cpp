/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.hh"
#include "ui_mainwindow.h"
#include "view.hh"
#include "constants.hh"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    filetab = new FileTab(this);
    this->setCentralWidget(filetab);

    createActions();
    createMenus();
}

MainWindow::~MainWindow()
{
    delete filetab;
    delete fileMenu;
    delete siteMenu;
    delete newSiteAct;
    delete newWorkerAct;
    delete openSiteAct;
    delete ui;
}


void MainWindow::createActions()
//! [17] //! [18]
{
    newSiteAct = new QAction(QIcon(UMapOrg::SiteIcon), tr("&NewSite"), this);
    newSiteAct->setShortcuts(QKeySequence::New);
    newSiteAct->setStatusTip(tr("Create new Site"));
    connect(newSiteAct, SIGNAL(triggered()), filetab, SLOT(newSiteTab()));

    newWorkerAct = new QAction(QIcon(UMapOrg::SiteIcon), tr("&NewWorker"), this);
    newWorkerAct->setStatusTip(tr("Create new Worker"));
    newWorkerAct->setDisabled(true);


    openSiteAct = new QAction(QIcon(UMapOrg::SiteIcon), tr("&OpenSite"), this);
    openSiteAct->setShortcuts(QKeySequence::Open);
    openSiteAct->setStatusTip(tr("Open Site"));
    connect(openSiteAct, SIGNAL(triggered()), filetab, SLOT(openSite()));
}

void MainWindow::createMenus()
{
 fileMenu = ui->mainMenuBar->addMenu(tr("&File"));
    fileMenu->addAction(openSiteAct);

    siteMenu = ui->mainMenuBar->addMenu(tr("&Site"));
    siteMenu->addAction(newSiteAct);
    siteMenu->addAction(newWorkerAct);
}
