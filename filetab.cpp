/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QFileDialog>
#include <QDebug>
#include <QMimeDatabase>
#include <QMessageBox>

#include "readjson.hh"
#include "filetab.hh"


FileTab::FileTab(MainWindow *parent) :
    QTabWidget(parent),
    window(parent)
{
    setTabsClosable(true);

    connect(this, SIGNAL(tabCloseRequested(int)),
            this, SLOT(onTabCloseRequested(int)));

    connect(this, SIGNAL(currentChanged(int)),
            this, SLOT(onTabSwitch(int)));
}

void FileTab::newSiteTab()
{
    //activates the close button

    currentView = new View(this);

    oldTabIndex = currentIndex();

    // add new tab and switch to it
    setCurrentIndex(addTab(currentView,
                           currentView->getSite()->getName()));

}

void FileTab::openSite()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open File"),
                                                    QDir::homePath(),
                                                    tr("db file(*.db);; json files(*.json);; All types(*.*)"));
    if(fileName.isEmpty())
        return;

    QMimeDatabase db;
    QMimeType type = db.mimeTypeForFile(fileName);

    if(type.inherits("application/x-sqlite3"))
    {
        qDebug() << type.name();
    }
    else if(type.inherits("application/json"))
    {
        newSiteTab();
        auto json = ReadJson(fileName);
//        auto sqlite = sqlitedb(fileName);
        json.setupSite(currentView->getSite());
        setTabText(currentIndex(),
                   currentView->getSite()->getName());
    }
    else
    {
        int ret = QMessageBox::warning(this, "Unsupported File",
                             "Extention type: " +type.name()
                             +"\nis not supported by uMapOrg\n",
                             QMessageBox::Retry | QMessageBox::Cancel);
        if(ret == QMessageBox::Retry)
            openSite();
    }
}

void FileTab::onTabCloseRequested(int index)
{
    delete widget(index);
    //removeTab(index);
}

void FileTab::onTabSwitch(int index)
{
    qDebug() << oldTabIndex;

    // do not disconnect if we only just created the first tab
    if(oldTabIndex != -1)
    {
        View *previousView = static_cast<View*>(widget(oldTabIndex));
        if(previousView != nullptr)
            disconnect(window->newWorkerAct, SIGNAL(triggered()),
                       previousView->getSite(), SLOT(slotAddWorker()));
    }

    currentView = static_cast<View*>(widget(index));

    if(currentView != nullptr)  // check if there is no tab at all
    {
        connect(window->newWorkerAct, SIGNAL(triggered()),
                currentView->getSite(), SLOT(slotAddWorker()));

        window->newWorkerAct->setEnabled(true);
    }
    oldTabIndex = index;
}
