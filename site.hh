/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SITE_HH
#define SITE_HH
#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QList>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include "worker.hh"
#include "view.hh"

class View;

struct CoreSite
{
    QString mSiteName;
    QString mSiteDescription;
    QImage mImage;
    int mNoOfWorkers{0};
    int mNoOfMembers{0};
    double mDensity{0.0};
};
// class for customization
class Site :public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:

    Site(View * v, QGraphicsItem * parent = 0);
    ~Site();

    QRectF boundingRect() const;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget);

    Worker *addWorker(QString name = "", bool newOne = false);
    inline void setImage(const QString &path);
    void setSiteToolTip();
    void setName(const QString &name);
    void setDescription(const QString &description);

    void setWorkersPosition(int rowSize = 6);
    void remWorker(Worker *worker);

    QString getName() const;
public slots:
    void slotAddWorker();

private:
    CoreSite pCore;
    View *pView = nullptr;
    QMenu *pRightClickMenu = nullptr;

    QList<Worker *> mListWorkers;

    const double itemWidth = 130.0;
    void setRowPosition(int start, int end, int rowNumber);
    void contextMenu(QGraphicsSceneContextMenuEvent *event);

protected:
    // overriding mouse events
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
};


#endif // SITE_HH
