/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMap.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMap. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef READJSON_HH
#define READJSON_HH
#include <QDebug>
#include <QString>
#include <QByteArray>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QJsonArray>

#include "site.hh"

class Site;


class ReadJson
{
public:
    ReadJson();
    ReadJson(const QString &fileName);
    ~ReadJson();
    bool readFile(const QString &fileName);

    void setupSite(Site *site);
private:
    QFile *file = nullptr;
    QJsonDocument jDocument;
    QByteArray fileByteArray;
};

#endif // READJSON_HH
