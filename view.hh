/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIEW_HH
#define VIEW_HH
#include <QGraphicsView>
#include <QGraphicsScene>
#include "filetab.hh"
#include "site.hh"

class Site;
class FileTab;

class View : public QGraphicsView
{
    Q_OBJECT
public:
    explicit View(FileTab *parent);
    ~View();
    Site *createSite();
    Site *getSite();

public slots:
    void slotTest();

private:
    Site *site = nullptr;
};

#endif // VIEW_HH
