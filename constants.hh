/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CONSTANTS_HH
#define CONSTANTS_HH
#include <QString>

namespace UMapOrg
{
static const QString AppIcon(":/data/icon.svg");
static const QString WorkerIcon(":/data/worker.png");
static const QString SiteIcon(":/data/site.png");
static const QString RedDot(":/data/red_dot.png");
static const QString GreenDot(":/data/green_dot.png");
static const QString AddWorker(":/data/add_worker.png");

}
#endif // CONSTANTS_HH
