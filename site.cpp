/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include "site.hh"
#include "constants.hh"

Site::Site(View * v, QGraphicsItem * parent) :
    QGraphicsItem(parent),
    pView(v)
{
    setFlag(ItemIsMovable);
    pView->scene()->addItem(this);
    setImage(UMapOrg::SiteIcon);

}

Site::~Site()
{
    //delete pCore;
    delete pRightClickMenu;
    for(auto w: mListWorkers)
        delete w;
}



QRectF Site::boundingRect() const
{
    // outer most edges
    return QRectF(0,0,150,150);
}

void Site::setImage(const QString &path)
{
    pCore.mImage = QImage(path);
}

void Site::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);
    QRectF rect = boundingRect();

    painter->drawImage(rect, pCore.mImage);
    QPen pen(Qt::black , 15);
    painter->setPen(pen);
    painter->drawText(rect, Qt::AlignHCenter , pCore.mSiteName);
}

void Site::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
}

void Site::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    contextMenu(event);
}

void Site::contextMenu(QGraphicsSceneContextMenuEvent *event)
{
    if(pRightClickMenu == nullptr)
    {
        pRightClickMenu = new QMenu(this->pView);
        QAction *addWorkerAct = pRightClickMenu->addAction("add Worker");
        addWorkerAct->setIcon(QIcon(UMapOrg::AddWorker));
        //menu->addAction("Action 2");

        connect(addWorkerAct, SIGNAL(triggered()),
                this, SLOT(slotAddWorker()));
    }
    pRightClickMenu->popup(event->screenPos());
}

void Site::slotAddWorker()
{
    addWorker("First Last", true);
}

void Site::setSiteToolTip()
{
    QString tooltip;
    tooltip.append("name: " + pCore.mSiteName + "\n"
                   + "Description: " + pCore.mSiteDescription);
    setToolTip(tooltip);
}

void Site::setName(const QString &name) { pCore.mSiteName = name;}

void Site::setDescription(const QString &description) { pCore.mSiteDescription = description;}

QString Site::getName() const {return pCore.mSiteName;}

// TODO set workers position
void Site::setWorkersPosition(int rowSize)
{
    int rowNumber = 1;
    for (int start = 0; start < mListWorkers.size(); start += rowSize)
    {
        int end = qMin(start + rowSize, mListWorkers.size());
        setRowPosition(start, end, rowNumber++);
    }
}

void Site::setRowPosition(int start, int end, int rowNumber)
{
    int noOfElements = end-start;
    qreal gap = 20.0;
    qreal lineWidth = (itemWidth * noOfElements) + (gap * (noOfElements - 1));
    //qDebug() << lineWidth << "\n";
    qreal x =  -(lineWidth/2) + 65;
    qreal y = 200.0 * rowNumber;
    for(; start < end; start++)
    {
        mListWorkers.at(start)->setPos(x, y);
        //qDebug() << "x= " << x << "y= " << y << "\n";
        x += itemWidth + gap;
    }
}

Worker *Site::addWorker(QString name, bool newOne)
{
    Worker *newWorker = new Worker(this);
    Q_ASSERT(newWorker != nullptr);

    newWorker->setName(name);
    //scene->addItem(newWorker);
    mListWorkers.append(newWorker);

    if(newOne == true)
    {
        if(newWorker->editWorker())
        {
            setWorkersPosition();
        }
        else
        {
            remWorker(newWorker);
            return nullptr;
        }
    }

    return newWorker;
}

void Site::remWorker(Worker *worker)
{
    mListWorkers.removeOne(worker);
    delete worker;
}
