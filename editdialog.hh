/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EDITDIALOG_HH
#define EDITDIALOG_HH

#include <QDialog>
#include <QPixmap>
#include "worker.hh"

class Worker;
namespace Ui
{
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(Worker &worker, QWidget *parent = nullptr);
    ~EditDialog();

private:
    Ui::EditDialog *ui;
    QGraphicsScene *scene;
    QPixmap pixmap;
    QImage image;
private slots:
    void onAccepted(Worker &worker);
    void onPhotoButtonClicked();
};

#endif // EDITDIALOG_HH
