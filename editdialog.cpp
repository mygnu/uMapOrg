/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include "editdialog.hh"
#include "ui_editdialog.h"
#include <QDebug>
#include <QFileDialog>


EditDialog::EditDialog(Worker &worker, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
    setModal(true);
    scene = new QGraphicsScene(this);
    ui->graphicsView->setScene(scene);
    pixmap = QPixmap::fromImage(worker.getImage()).scaled(135,135);
//    pixmap = pixmap.scaled(135,135);// view area is 140x140

    scene->addPixmap(pixmap);

    ui->MemberCBox->setCheckState( worker.isMember() == true
                                   ? Qt::Checked : Qt::Unchecked);
    ui->nameLineEdit->setText(worker.getFullName());
    ui->notesTextEdit->setText(worker.getAbout());


    connect(ui->pushButton_Ok, SIGNAL(clicked()),
            this, SLOT(onAccepted(worker)));
    connect(ui->imageButton, SIGNAL(clicked()), this, SLOT(onPhotoButtonClicked()));
//    connect(ui->nameLineEdit, SIGNAL(returnPressed()),
//            this, SLOT(onAccepted()));

#ifdef ANDROID
    this->showMaximized();
#endif

}


void EditDialog::onAccepted(Worker &worker)
{
    QString s = ui->nameLineEdit->text();

    worker.setName(s);

    s = ui->notesTextEdit->toPlainText();
    worker.setMember(ui->MemberCBox->checkState() == Qt::Checked);
    worker.setAbout(s);
    if(!image.isNull())
        worker.setImage(image);
    worker.setWorkerToolTip();
}

void EditDialog::onPhotoButtonClicked()
{
    QString photoPath = QFileDialog::getOpenFileName(this);

    pixmap = QPixmap(photoPath);
    if(pixmap.isNull())
        return;
    QGraphicsItem *_item = scene->items().at(0);
    scene->removeItem(_item);
    delete _item;
    scene->addPixmap(pixmap.scaled(135, 135));
    image = QImage(photoPath);
}

EditDialog::~EditDialog()
{
    delete scene;
    delete ui;
}

