/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include "constants.hh"
#include "worker.hh"
#include "editdialog.hh"

Worker::Worker(QGraphicsItem *parent) :
    QGraphicsItem(parent)

{
   pCore.image = QImage(":data/avatar_f_200x200.png");
   setFlag(ItemIsMovable);
}

Worker::~Worker(){}

    // outer most edges
QRectF Worker::boundingRect() const
{
    return QRectF(0,0,130,150);
}


void Worker::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                   QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QRectF rect = boundingRect();
    painter->drawImage(QRectF(0,20,130,130), pCore.image);
    if(isMember())
        painter->drawImage(QRectF(120, 120, 20,20), QImage(UMapOrg::GreenDot));
    else
        painter->drawImage(QRectF(120, 120, 20,20), QImage(UMapOrg::RedDot));

    QPen pen(Qt::black , 15);
    painter->setPen(pen);
    painter->drawText(rect, Qt::AlignHCenter, getFullName());
}

QString Worker::getFullName() const { return pCore.mFirstName +" "+ pCore.mLastName;}

QString Worker::getFirstName() const { return pCore.mFirstName;}

QString Worker::getLastName() const { return pCore.mLastName;}

QString Worker::getAbout() const { return pCore.mAbout; }

void Worker::setAbout(const QString &about) { pCore.mAbout = about;}

void Worker::setName(const QString &FirstLastNameWithSpace)
{
    auto nameList = FirstLastNameWithSpace.split(" ", QString::SkipEmptyParts);
    pCore.mFirstName = nameList.at(0);
    if(nameList.count() > 1)
        pCore.mLastName = nameList.at(nameList.count() - 1); // only use the last name
    else pCore.mLastName = "";
}

void Worker::setName(const QString &FirstName, const QString &LastName)
{
    pCore.mFirstName = FirstName;
    pCore.mLastName = LastName;
}

void Worker::setImage(QString imagePath) { pCore.image = QImage(imagePath);}

void Worker::setImage(QImage image) { pCore.image = image; }

QImage Worker::getImage() const { return pCore.image; }

bool Worker::isMember() { return pCore.mIsMember; }

void Worker::setMember(bool value) { pCore.mIsMember = value; }

void Worker::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if(editWorker())
        update();

    QGraphicsItem::mouseDoubleClickEvent(event);
}

/**
 * sets the tooltips for the worker according to the available informatin
 **/

void Worker::setWorkerToolTip()
{
    QString tooltip = QString("Name: %1 \nAbout: %2 \nMember: %3 \n").
            arg(getFullName(),getAbout(), (isMember() == true ? "yes" : "no") );
    setToolTip(tooltip);
}
/**
 * Opens up the edit dialog to edit worker
 **/
bool Worker::editWorker()
{
    EditDialog eDialog(*this);
    if(eDialog.exec() == QDialog::Accepted)
    {
        setWorkerToolTip();
        return true;
    }
    return false;
}
