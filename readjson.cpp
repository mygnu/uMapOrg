/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMap.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMap. If not, see <http://www.gnu.org/licenses/>.
*/

#include "readjson.hh"
#include "worker.hh"

ReadJson::ReadJson()
{
}

ReadJson::ReadJson(const QString &fileName)
{
    qDebug() << fileName;
    if(readFile(fileName))
    {
//        qDebug() << "file opened";
    } else {
        delete this;
    }
}

ReadJson::~ReadJson()
{
    file->close();
    delete file;
//    qDebug() << "object deleted";
}

bool ReadJson::readFile(const QString &fileName)
{
    file = new QFile();
    file->setFileName(fileName);

    bool success = file->open(QIODevice::ReadWrite | QIODevice::Text);
    if(!success)
        return false;

    fileByteArray = file->readAll();
    if(fileByteArray.isNull())
        return false;

//    qDebug() << fileByteArray; // prints the whole file
    return true;
}


void ReadJson::setupSite(Site *site)
{
    jDocument = QJsonDocument::fromJson(fileByteArray);
    // A JSON object is a list of key value pairs, where the keys are unique
    // strings and the values are represented by a QJsonValue.
    QJsonObject obj = jDocument.object();
    QJsonValue jsite = obj.value(QString("Site"));

    if(jsite.isObject())
    {
        QJsonObject item = jsite.toObject();

        site->setName(item["Name"].toString());
        site->setDescription(item["Notes"].toString());

        QJsonArray jworkers = item["Workers"].toArray();

        for(auto i:jworkers)
        {
            QJsonObject jworker = i.toObject() ;
            Worker *worker = site->addWorker(jworker["Name"].toString());
            worker->setAbout(jworker["Notes"].toString());
            worker->setMember(jworker["isMember"].toBool());
            worker->setWorkerToolTip();
//            qDebug() << jworker["Name"].toString();
//            qDebug() << jworker["Notes"].toString();
//            qDebug() << jworker["ID"].toDouble();
//            qDebug() << jworker["isMember"].toBool();
        }
        site->setWorkersPosition();
    }
}
