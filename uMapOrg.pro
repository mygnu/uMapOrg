#-------------------------------------------------
#
# Project created by QtCreator 2014-11-25T01:33:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = umaporg
TEMPLATE = app


SOURCES += \
    main.cpp\
    mainwindow.cpp \
    worker.cpp \
    editdialog.cpp \
    view.cpp \
    site.cpp \
    filetab.cpp \
    readjson.cpp

HEADERS += \
    mainwindow.hh \
    worker.hh \
    editdialog.hh \
    view.hh \
    site.hh \
    constants.hh \
    filetab.hh \
    readjson.hh

FORMS   += \
    mainwindow.ui \
    editdialog.ui

CONFIG += mobility
CONFIG += c++11

MOBILITY =

RESOURCES += \
    resources.qrc

