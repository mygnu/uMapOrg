/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#include "view.hh"
#include <QDebug>

View::View(FileTab *parent) : QGraphicsView(parent)
{
    this->setScene(new QGraphicsScene(this));
    site = new Site(this);
    setRenderHint(QPainter::HighQualityAntialiasing, true);

}

View::~View()
{
    delete site;
}

Site *View::getSite() { return site;}

void View::slotTest()
{
    qDebug() << "New Worker Clicked";
}
