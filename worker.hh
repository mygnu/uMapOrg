/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WORKER_HH
#define WORKER_HH

#include <QGraphicsItem>
#include <QPainter>
#include <QImage>

struct CoreWorker
{
    QString mFirstName;
    QString mLastName;
    QString mTitle;
    QString mAbout;
    QImage image;
    bool mIsMember = false;
    bool mIsActive = false;
    bool mIsLeader = false;
    bool mIsHostile = false;
};

// class for customization
class Worker :public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    // Constructor
    Worker(QGraphicsItem *parent = 0);
    ~Worker();

    // overriding boundingRect()
    QRectF boundingRect() const;

    // overriding paint()
    void paint(QPainter * painter,
               const QStyleOptionGraphicsItem * option,
               QWidget * widget);

    // returns the site this worker is attached to
    QString getFullName() const;
    QString getFirstName() const;
    QString getLastName() const;
    QString getAbout() const;
    QImage getImage() const;
    bool isMember();
    void setMember(bool value);
    void setAbout(const QString &about);
    void setName(const QString &FirstLastNameWithSpace);
    void setName(const QString &FirstName, const QString &LastName);
    void setImage(QString imagePath);
    void setImage(QImage image);



    void setWorkerToolTip();
    bool editWorker();

signals:
    void changed(Worker *w);

protected:
    // overriding mouse events
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

private:
    QString photoPath = ":/images/avatar_f_200x200.png";

    CoreWorker pCore;
    friend class EditDialog;
};

#endif // WORKER_HH
