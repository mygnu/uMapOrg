/*
* Copyright © 2014 Mygnu <me@mygnu.me>
*
*
*
* This file is part of uMapOrg.
*
* uMap is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Final Term is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with uMapOrg. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILETAB_HH
#define FILETAB_HH
#include <QTabWidget>
#include <QGraphicsScene>

#include "mainwindow.hh"
#include "view.hh"
#include "site.hh"

class View;
class MainWindow;


class FileTab : public QTabWidget
{
    Q_OBJECT

public:
    FileTab(MainWindow * parent);
    const MainWindow *window =0;
    View *currentView= 0;


public slots:
    void newSiteTab();
    void openSite();
private:
    int oldTabIndex;

private slots:
    void onTabCloseRequested(int index);

    void onTabSwitch(int index);
};


#endif // FILETAB_HH
